
<!-- Various tags needed for mobile, plus general meta tags -->
<meta charset="UTF-8" />
<title><?php echo $dataObject->title; ?></title>
<meta name="description" content="<?php echo $dataObject->metadesc; ?>">
<!-- Set the viewport for mobile -->
<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,minimum-scale=1,user-scalable=no" />
<meta name="apple-mobile-web-app-capable" content="yes" /> 
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" >