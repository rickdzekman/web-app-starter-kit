<?php

/**
 * This dummy bit of code meant to simulate some sort of server side work.
 * Here it acts acts as a placeholder for your back end content and it
 * could be replaced with any back-end technology.
 *
 */
usleep(400000); //simulate some server load time so that we see the loading screen

/**
 * Google Analytics is loaded conditionally with this event, allowing you to optionally
 * load a "dev" GA property. Useful for testing things like event tracking without impacting
 * your "real" GA data. This can be useful in the back-end to conditionally load other things,
 * such as database configurations for development environments.
 *
 */
define('ENVIRONMENT', 'development');

/**
 * Data object that populates the front end.
 *
 */
class DataObject {

    public $title = 'Title';
    public $metadesc = 'Description goes here';
    public $content = '<h2>Content here</h2><p>And here....</p>';

    function __construct($t, $m, $c) {
        $this->title = $t;
        $this->metadesc = $m;
        $this->content = $c;
    }

}

/**
 * Placeholder content
 *
 */
include 'documentation/about.php';
include 'documentation/about-requirements.php';
include 'documentation/about-general.php';
include 'documentation/backbutton.php';
include 'documentation/ajax.php';
include 'documentation/ajax-class.php';
include 'documentation/ajax-popstate.php';
include 'documentation/access.php';
include 'documentation/access-matters.php';
include 'documentation/access-content.php';
include 'documentation/access-legacy.php';
include 'documentation/other.php';
include 'documentation/license.php';

$dataObjects = array(
    '404' => new DataObject('Error 404', 'Page not found', '<h2>Page not found</h2>'),
    'home' => new DataObject('home', 'This is the home page', '
			<img src="http://placehold.it/1000x150" width="100%" height="auto">
			<h2>home page content here</h2>
			<p>And here....</p>
			'),
    'about' => new DataObject('About', 'About this starter kit', $about),
    'about-general' => new DataObject('Why this starter kit', 'Why this starter kit', $aboutGeneral),
    'about-requirements' => new DataObject('Requirements', 'Requirements for running the kit', $aboutRequire),
    'ajax' => new DataObject('Using Ajax', 'Using ajax with this starterkit', $ajax),
    'ajax-class' => new DataObject('The ajax class', 'The ajax class', $ajaxClass),
    'backbutton' => new DataObject('Browser Back Button', 'The browser back button', $backbutton),
    'ajax-popstate' => new DataObject('History Popstate', 'The popstate problem in some browsers', $ajaxPopstate),
    'accessibility' => new DataObject('Accessibility', 'Accessibility overviews', $access),
    'accessibility-matters' => new DataObject('Why Accessibility Matters', 'Accessibility matters', $accessMatters),
    'accessibility-structure' => new DataObject('Content Structure for Accessibility', 'Content Structure for Accessibility', $accessContent),
    'accessibility-legacy' => new DataObject('Older Browsers', 'Support for older browsers', $accessLegacy),
    'other' => new DataObject('Other Useful Components', 'Other Useful Components', $other),
    'license' => new DataObject('MIT License', 'License for this code', $license)
);

/**
 * URL paths
 *
 */
$dataObject = $dataObjects['home'];
if (isset($_GET['page'])) {
    if (array_key_exists($_GET["page"], $dataObjects)) {
        $dataObject = $dataObjects[$_GET['page']];
    } else {
        $dataObject = $dataObjects['404'];
    }
}
?> 
