<!-- Touch icons -->

<link rel="apple-touch-icon" sizes="114x114" href="/assets/icons/apple-touch-icon-114x114-precomposed.png" />
<link rel="apple-touch-icon" sizes="72x72" href="/assets/icons/apple-touch-icon-72x72-precomposed.png" />
<link rel="apple-touch-icon" sizes="144x144" href="/assets/icons/apple-touch-icon-144x144-precomposed.png" />
<link rel="apple-touch-icon" sizes="57x57" href="/assets/icons/apple-touch-icon-57x57-precomposed.png" />
<link rel="apple-touch-icon" href="/assets/icons/apple-touch-icon-precomposed.png" />

<!-- iPhone 3GS, 2011 iPod Touch -->
<link rel="apple-touch-startup-image" href="/assets/startup/startup-320x460.png" media="screen and (max-device-width : 320px)" />

<!-- iPhone 4, 4S and 2011 iPod Touch -->
<link rel="apple-touch-startup-image" href="/assets/startup/startup-640x920.png" media="(max-device-width : 480px) and (-webkit-min-device-pixel-ratio : 2)" />

<!-- iPhone 5 and 2012 iPod Touch -->
<link rel="apple-touch-startup-image" href="/assets/startup/startup-640x1096.png" media="(max-device-width : 548px) and (-webkit-min-device-pixel-ratio : 2)" />

<!-- iPad landscape -->
<link rel="apple-touch-startup-image" sizes="1024x748" href="/assets/startup/startup-1024x748.png" media="screen and (min-device-width : 481px) and (max-device-width : 1024px) and (orientation : landscape)" />

<!-- iPad Portrait -->
<link rel="apple-touch-startup-image" sizes="768x1004" href="/assets/startup/startup-768x1004.png" media="screen and (min-device-width : 481px) and (max-device-width : 1024px) and (orientation : portrait)" />

<!-- iPad (Retina, portrait) -->
<link rel="apple-touch-startup-image"
      media="(device-width: 768px)
      and (orientation: portrait)
      and (-webkit-device-pixel-ratio: 2)"
      href="/assets/startup/startup-1536x2008.png" />

<!-- iPad (Retina, landscape) -->
<link rel="apple-touch-startup-image"
      media="(device-width: 768px)
      and (orientation: landscape)
      and (-webkit-device-pixel-ratio: 2)"
      href="/assets/startup/startup-2048x1496.png" />

<!-- favicon -->
<link rel="shortcut icon" type="image/x-icon" href="/assets/favicons/favicon.ico" />

<!-- Open Graph -->
<meta property="og:image" content="/assets/startup/startup-1024x768.png" />
<!--<meta property="fb:app_id" content="" />-->
<meta property="og:type" content="website" />
<meta property="og:title" content="<?php echo $dataObject->title; ?>" /> 
<meta property="og:site_name" content="<?php echo $dataObject->metadesc; ?>" />


<!-- normalize / main are from modernizr-->
<link rel="stylesheet" type="text/css" href="/assets/css/normalize.min.css" />
<link rel="stylesheet" type="text/css" href="/assets/css/main.css" />
<!-- custom.css includes the default styles from this starter kit. Add custom css there -->
<link rel="stylesheet" type="text/css" href="/assets/css/custom.css" />

<!--
	frameworks.min.js contains: jquery-1.8.3, History, Respond and Modernizr.
	Have to load Modernizr in the header or else the .no-js class
	kicks in after page load and it looks weird.
-->
<script src="/assets/js/frameworks.min.js" type="text/javascript"></script>