			
        </div>
		<!-- sticky footer acheived with flexbox -->
		<footer>
			<p>&copy Copyright theCompany</p>
		</footer>

<!-- JS to show the "add this app to your home screen" pop-up on iOS.
	NB. this file has been customised to change autostart: false -->
<script src="/assets/js/add2home.js" type="text/javascript"></script>

<!-- this is all the custom javascript for this app. -->
<script src="/assets/js/custom.js" type="text/javascript"></script>


<?php
//Generate different analytics depending on the environment
switch (ENVIRONMENT) {
    case 'development':
?>
<!-- DEVELOPMENT ANALYTICS -->
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-XXXXXXXX-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>   
<?php        
        break;

    case 'production':
?>
<!-- PRODUCTION ANALYTICS -->
<script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-XXXXXXXX-1']);
            _gaq.push(['_trackPageview']);

            (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();

        </script>
<?php  
        break;

    default:
        echo '<!-- The application environment is not set correctly -->';
}
?>
		
    </body>
</html>