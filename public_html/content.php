<aside>
    <?php include 'sidebar.php'; ?>
</aside><!-- fighting the pointless extra space between inline-block elements
--><section aria-live="polite">
    <!-- The #main div is used by AJAX to replace new content -->
    <div id="main">
        <?php echo '<h1>' . $dataObject->title . '</h1>'; ?>
        <?php echo $dataObject->content; ?>
    </div>
</section>