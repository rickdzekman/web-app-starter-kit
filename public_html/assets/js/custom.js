/**
 * This custom javascript serves several useful purposes for web-apps:
 *      (a) A simple custom toggle menu for smaller screens
 *      (b) Show the "Add to Home" popup only to users on the home page
 *          that way users arriving from Google won't bookmark sub-pages
 *      (c) And most importantly an implementation of the History.js plugin
 *          This will ensure that:
 *              - Pages load dynamically using AJAX
 *              - Users are able to press back/forward on their browser
 *              - Pressing back/forward re-calls the pages with AJAX
 *              - A link that takes you to the same page acts as a dynamic
 *                refresh without adding another history event
 *              - Loading screen that comes up while the user waits
 *              - Cancelling an AJAX request if the user presses back
 *                more than once
 *              - AJAX errors do not leave the app hanging on a loading screen
 *              - An attempt to handle the initial popstate on some browsers
 *                without needing a workaround like a set-timeout on the first
 *                AJAX request.
 *
 */

/* ---------------------------
 Global Variables
 --------------------------- */

/*
 * Keep track of any AJAX requests in progress. If the user hits back more
 * than once it will trigger a second AJAX request so the first one needs
 * to be cancelled.
 */
var getInProgress = false;
var request;

/*
 * Don't show the add2home bubble more than once.
 */
var shownAddHome = false;

/* ---------------------------
 Menu
 --------------------------- */

$(document).ready(function() {
    $('.toggle-menu').before('<a href="#" class="menu-toggle" role="button">Menu</a>');
    if ($('.menu-toggle').is(':visible')) {
        $('.toggle-menu').addClass('toggleable');
    }

    $('.menu-toggle').on('click', function() {
        menuToggle($(this));
        return false;
    });
	
	$('.toggleable a').on('click',function(){
		menuToggle($('.menu-toggle'));
	});
});

function menuToggle(that) {
	$('.toggle-menu').slideToggle(function() {
		that.toggleClass('showmenu');
	});

	if (that.hasClass('open')) {
		that.empty().removeClass('open').append("Menu");
	} else {
		var x = that.width();
		that.empty().addClass('open').append("X").width(x);
	}
}

/* ---------------------------
 AJAX Error Handling
 --------------------------- */

/*
 * In case of an AJAX error this general message replaces the content. Use
 * $.ajax with error callback if more granularity is required.
 */
$(document).ajaxError(function() {
    $('#main').html("<h2>Something went wrong... please try again</h2>");
    getInProgress = false;
    hideLoad();
});

/* ---------------------------
 Document ready function
 --------------------------- */


$(document).ready(function() {

    /*
     * If any actions need to be performed on window resize (or for that matter
     * orientation change) then it is often worth performing those same actions
     * after an AJAX page laod. 
     */
    $(window).resize(function() {
        resizeWindow();
    }).trigger('resize');

    hideLoad(); //if JS is enabled the loading bars are display:block till doc ready
    checkAddHome(); //check if the user has started on the home page and show them the add to home button

});

$(window).load(function() {
    $(window).resize();
});

/* ---------------------------
 Functions
 --------------------------- */


/**
 * If we've never shown the user the add to home button AND this is 
 * the home page then show them the add to home button
 * 
 */
function checkAddHome() {
    if (!shownAddHome) {
        if (window.location.pathname == "/") {
            addToHome.show();
            shownAddHome = true;
        }
    }
}

/**
 * Placeholder function for any window resize tweaks
 * Also useful for orientation change as the tweaks tend to be the same
 */
function resizeWindow() {
    var that = $('.toggle-menu');
    if ($('.menu-toggle').is(':visible')) {
        that.addClass('toggleable');
    } else {
        that.removeClass('toggleable');
        that.removeAttr('style');
        $(this).removeClass('showmenu');
    }
}

/**
 * Call this whenever you need to hide the loading bars
 */
function hideLoad() {
    $('#floatingBarsContainer').fadeOut(400);
}

/**
 * When comparing the URL in an <a> tag vs the State.hash function, the
 * URL that's already in the address bar is URL encoded. Decode both to
 * compare. Includes repalcement for spaces as plusses.
 */
function urldecode(str) {
    return decodeURIComponent((str + '').replace(/\+/g, '%20'));
}

/**
 * Separate out clicks from other ajax page loads like form submissions
 */
function ajaxClick(page) {
    historyPush(page);
}

/**
 * Separate out clicks from other ajax page loads like form submissions
 * NB. This function is called if the current URL matches the target URL
 */
function ajaxRefresh(page) {
    historyRefresh(page);
}

/**
 * If the user clicked a link to take them to the same page then there is
 * no need to push a change to the browser state.
 */
function historyRefresh(page) {
    dynamicLoadPage(page);
}

/**
 * Use History.js to change browser state to the target URL
 * This triggers the statechange function that will dynamically load
 * the page
 */
function historyPush(page) {
    History.pushState({}, null, page);
}

/**
 * Abort any in progress AJAX requests, then send an AJAX call to the target
 * URL. Fetch the #main div and replace the content with a transition.
 * 
 * NB1. If your server side code is configured to accommodate this, use this
 * section to change the URL and tell your server not to load the whole
 * target page, but just the DOM elements that you need to insert. Then the
 * $(data).find('#main') will not be required (though you will need to
 * hide the <title> tag in a [data] attribute.
 * 
 * NB2. Replace the get request with JSON/JSONP 
 */
function dynamicLoadPage(page) {
    if (getInProgress) {
        request.abort();

        getInProgress = false;
    }

    $('#floatingBarsContainer').fadeIn();

    getInProgress = true;
    request = $.get(page, function(data) {
        var matches = data.match(/<title>(.*?)<\/title>/);
        var spUrlTitle = matches[1];
        $(document).attr('title', spUrlTitle);
        _gaq.push(['_trackPageview', page]);

        var mainDiv = $(data).find('#main');

        $("#main").fadeOut(100).hide();
        $("html, body").animate({
            scrollTop: 0
        }, "slow");
        getInProgress = false;
        hideLoad();
        $("#main").html(mainDiv.html()).slideDown(500);
        $(window).resize();
		//check if the user has selected on the home page and whether to show them the add home popup
        checkAddHome();

    });
}

/* ---------------------------
 History.js implementation
 --------------------------- */


/**
 * This whole  function window undefined thing is (dubiously) suppose 
 * to make accessing the $(window) object faster.
 * 
 */
(function(window, undefined) {

    // Prepare
    var History = window.History; // Note: We are using a capital H instead of a lower h
    if (!History.enabled) {
        // History.js is disabled for this browser.
        // This is because we can optionally choose to support HTML4 browsers or not.
        return false;
    }


    /**
     * The statechange triggers the AJAX load, rather than an AJAX load
     * triggering a state change.
     */
    History.Adapter.bind(window, 'statechange', function() { // Note: We are using statechange instead of popstate
        var State = History.getState(); // Note: We are using History.getState() instead of event.state
        dynamicLoadPage(State.url);
    });

    /*
     * Add an ajax class to a every <a> tag you want to load dynamically.
     * If your app is running as a web-app in iOS, any links that are not
     * marked with an ajax class will open in a new Safari window.
     * 
     * This also checks to see if the target destination is the same as the
     * current page. If so it needs to do an AJAX refresh rather than push.
     */
    $(document).on('click', 'a.ajax', function(event) {
        event.preventDefault();
        var page = $(this).attr('href');
        var State = History.getState();
        if (urldecode(page) === urldecode(State.hash)) {
            ajaxRefresh(page);
        } else {
            ajaxClick(page);
        }

        return false;
    });

    /*
     * The uphill battle against some browsers' initial popstate
     */
    var State = History.getState();
    History.getState();
    if (urldecode(window.location) !== urldecode(State.url)) {
        window.location = State.url;
    } else {
        History.replaceState({}, State.title, State.url);
    }

})(window);