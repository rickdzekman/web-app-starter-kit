<!-- simple navigation that is toggle capable. -->
<header role="banner">
    <nav role="navigation">
            <a href="/" class="ajax logo">App Logo</a>
            <!--<div class="menu-button">Menu</div>-->
            <div class="toggle-menu">
                    <a href="/page/about" class="ajax">About</a>
                    <a href="/page/ajax" class="ajax">AJAX</a>
                    <a href="/page/accessibility" class="ajax">Accessibility</a>
                    <a href="/page/other" class="ajax">Other</a>
                    <a href="/page/license" class="ajax">License</a>
            </div>
    </nav>
</header>