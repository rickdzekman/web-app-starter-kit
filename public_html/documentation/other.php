<?php
$other = '
<h2>Google Analytics</h2>
<p>First of all a Google Analytics "push" event is already placed in the dynamicLoadPage(page) function enabling you to track 
people visiting pages on your site dynamically just like you would any other page view in Analytics.</p>
<p>Second of all the php side of code includes a variable defining the current "state" of the application as development. Then when 
the Google Analytics code is loaded it has the option of loading a second, alternative analytics snippet. This allows you to test all 
of your analytics code and the AJAX environment in a local setting without dirtying your true analytics results. Consider utilising the 
equivalent of this in whatever back-end framework that you use.</p>
<h2>Icons, Load screens and more</h2>
<h2>Tweaking the Add2Home Plugin</h2>
<p>Every variant of touch icon and web-app loading screen is included, as well as the funcionality needed for people to be able to save 
your web-app to their mobile desktop and load it full screen. Check out the assets folder to see everything.</p>
<p>The Add2Home plugin provides a nice little widget that detects iOS users and presents them with a small notification that they
can download this app to their home screen. By default this loads automatically as soon as a user loads your page. It is intended
that you would only include it on the pages of your site you want users to bookmark (namely the home page).</p>
<p>Unfortunately most people use page templates and we are no exception. Further, most of our users come from Google and land
directly on location search results or individual venues. This means that they are bookmarking particular pages rather than
the "app" itself.</p>
<p>This starter kit not only provides a check for whether or not the user is on the home page or not, but also integrates
this with the dynamic page loads functionality.</p>
<h2>Simple collapsible menu</h2>
<p>A very simple collapsible menu is included in this app that works with javascript. This has been built using progressive enhancement 
such that:</p>
<ul>
	<li>Javascript inserts the "menu" button on page load, preventing it from showing up for users that do not have javascript</li>
	<li>The toggleable class is added with javascript so that the menu is open by default</li>
        <li>All the styling is handled by CSS and not by Javascript (except the "toggleSlide" animation)</li>
        <li>The Javascript detects browser re-size and ensures the menu remains visible by resetting all classes on browser resize</li>
</ul>
<h2>Animated Loading</h2>
<p>A simple CSS3 loading screen is in place for users waiting between AJAX requests</p>
<h2>Flex-box Layout with Fallback</h2>
<h2>Flex-box and modernizr</h2>
<p>Because the flex box is not supported by all browsers this layout uses the modernizr .flexbox class to specifically target
those browsers that can support it. The basic layout is done with inline-block and depends heavily on the order of the code.
Depending on the purpose of your app it may be wise to swap around the sidebar and the content so that the sidebar is a right-hand
item for users without flex-box compatible browsers.</p>
<h2>Why flex-box?</h2>
<p>Flex box provides an incredibly simple way to:</p>
<ul>
	<li>Have the footer at the bottom of the page (the best other alternative is display: table-cell)</li>
	<li>Have a fixed size item (in our case it will be ads) that have to have a size in "px".
		The flexbox can then fill the remaining space.</li>
	<li>Use the "order" functionality to change the visual order from the code order.</li>
</ul>
'; ?>