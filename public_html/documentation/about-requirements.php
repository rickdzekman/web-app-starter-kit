<?php
$aboutRequire = '
<h2>Requirements</h2>
<h3>PHP</h3>
<p>While it is intended that you will replace the php code provided with any framework of your choosing, even with 
a different programming language, this demo itself requires PHP to work.</p>
<h3>URL Rewriting</h3>
<p>This demo comes with an .htaccess file that rewrites query strings to friendly URLs. Older browsers will have 
trouble making history.js work with query strings and it is strongly advised your application uses some kind of 
URL re-writing to assist in this process</p>
<h3>Non-Javascript Fallback</h3>
<p>This starter kit is built with the assumption that the user does not need Javascript enabled in order to use your 
web-app. Javascript here is used to enhance the user experience of the app rather than act as the only method of 
interaction for your users.</p>
<p>To that end the code expects that all URLs will terminate in a destination which can be navigated to directly.  
This allow your users to not only press the back and forward buttons on their browser but also use their browser 
refresh without negatively impacting their experience.</p>

'; ?>