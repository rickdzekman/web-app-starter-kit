<?php
$ajaxPopstate = '
<h2>Initial browser popstate</h2>
<p>If you are reading this there is a good chance that you\'ve had trouble using jQuery history before. The big problem 
is that the history API uses "popstate" to detect that the user has pressed back on their browser - and unfortunately 
some browsers insist on firing the "popstate" event <em>on initial load</em>.</p>
<p>This madness was a never end cause of frustration for us at whatistheretodo.com and the fix has been quite easy.</p>
<ol>
    <li>First - use history.js instead of jQuery history</li>
    <li>We use an older version of this plugin that seems to be more stable</li>
    <li>Finally you\'ll find this snippet of code in custom.js which takes care of this up-hill battle</li>
    <code>' . "
    /*<br />
     * The uphill battle against some browsers\' initial popstate<br />
     */<br />
    var State = History.getState();<br />
    History.getState();<br />
    if (urldecode(window.location) !== urldecode(State.url)) {<br />
        window.location = State.url;<br />
    } else {<br />
        History.replaceState({}, State.title, State.url);<br />
    }<br />
    " . '
    </code>
    <li>While that code should be sufficient there is a small problem in that history.js does not properly decode URLs and so UtF8 characters 
    are not properly supported. This meant that when it tries to match a UTF8 URL against the current browser URL it encounters a problem because 
    the characters look different to the browser. We have applied a fix to the plugin itself to address this (see the <a href="/page/backbutton" class="ajax">back button page</a> for more info).</li>
</ol>
'; ?>