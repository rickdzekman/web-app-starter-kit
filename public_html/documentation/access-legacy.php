<?php
$accessLegacy = '
<h2>Respond.js</h2>
<p>To help achieve support for older browsers this starter kit makes use of the respond.js framework for <= IE8.</p>
<h2>Using Modernizr</h2>
<p>Modernizr is used to target advanced CSS features that are not available to older browsers. The only one currently being used 
is the body class .flexboxlegacy to target browsers that support flexbox layouts.</p>
<h2>Mobile first CSS</h2>
<p>Because an older desktop browser can use a mobile site but an older mobile browser will struggle with a desktop site, the CSS 
has been provided mobile first.</>
'; ?>