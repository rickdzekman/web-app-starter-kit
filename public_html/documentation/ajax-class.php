<?php
$ajaxClass = '
<h2>The AJAX Class</h2>
<p>Simply attach class="ajax" to any link and it will automatically fetch the link dynamically.</p>
<h2>Extending the functionality</h2>
<p>To add extra functionality simply call the function dynamicLoadPage(page) in custom.js. For example - in whatistheretodo.com we use 
a search form which has an AJAX auto complete to identify a city. We capture the form submission with a "return false;" and instead 
process the information in the Javascript layer. Once we identify which page to send the user to we simply call dynamicLoadPage(newurl);</p>
'; ?>