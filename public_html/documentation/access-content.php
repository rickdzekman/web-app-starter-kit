<?php
$accessContent = '
<h2>Target CSS to ARIA Roles to encourage accessibility</h2>
<p>You can notice in the sample CSS provided that I make use of ARIA roles for CSS styling. For example the style on 
[role="button"] first of all allows you to target a link without having to give it a class="button" thus limiting how much 
styling you actually put in your HTML. Second it gives more semantic meaning to your HTML tags - role="button" says a lot to 
someone reading your code, not just about how it looks but about how it behaves. Finally it also encourages you to use 
the ARIA role making your website more accessible to assistive technologies.</p>

<h2>A really simple skip to content</h2>
<p>Take a look at the first bit of HTML in the code of this web-app and you will notice a skip to content link. Re-use as needed.</p>

<h2>Focus, outline and links</h2>
<p>You will notice something in this web-app... all links are underlined. Why? Links always require 2 ways to tell that they are a 
link - one of which is not colour. I prefer colour + underline (or sometimes colour + border-bottom). If you remove underlines from your 
links you are basically saying that you would happily trade off users with visual and/or cognitive impairment in exchange for things 
on your site looking more "pretty".</p>
<p>Next you\'ll find that all links that have :hover have the same styling applied to :focus. Further there is an "outline" style which has 
strong contrast against the rest of the site colours being used on focus. If you want to test the need for this, try navigating this demo 
using only your keyboard and see what happens.</p>

<h2>AJAX Accessibility</h2>
<p>This is one aspect of accessibility that I have simply not tested. It is here as a guide - further investigation is encouraged.</p>
<p>To tell screen readers that a particular part of the site is dynamic (i.e. the main content <section> tag) - I\'ve added added the following 
ARIA tag: <code>aria-live="polite"</code>. This tells a screen reader that the area is "live" and that it should not interrupt whatever the user was 
doing before the content changed. It should notify them subtly with a beep or some other noise.</p>

<h2>No JS Support</h2>
<p>This entire web-app assumes a fall-back to standard URL behaviour if javascript is not available. To encourage accessibility it is 
strongly encouraged that this behaviour is retained. This also has the addition of SEO benefit because now all of your pages can be indexed 
by a search engine crawler without having to worry about whether it supports javascript.</p>
'; ?>