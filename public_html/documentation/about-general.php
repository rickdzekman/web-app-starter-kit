<?php
$aboutGeneral = '
<h2>Building web-apps</h2>
<p>In building whatistheretodo.com I found a lack of comprehensive documentation or examples for how to build a rich, interactive web-app. 
The first version of whatistheretodo.com had many bugs and issues in the front end for both desktop and mobile users.</p>
<p>This starter kit provides the framework for what we had to do to re-design the whatistheretodo site. as well as all of the lessons 
that were learned along the way.</p>
<h2>What this is not</h2>
<p>This web-app starter kit is not meant to be a replacement to the likes of <a href="http://backbonejs.org/">Backbone</a> or <a href="http://emberjs.com/">Ember</a>. 
It is not meant to be a Javascript MVC framework or even a UI Kit like <a href="http://jquerymobile.com/">jQuery Mobile</a>.</p>
<p>This is not in any way a framework which will be continually updated or expanded upon and future releases should not be relied upon.</p>
<h2>What this is</h2>
<p>If you are building a web-app which has the majority of its logic handled server side rather than client side, then this starter kit 
will help you to create a front end interface for that application. It provides a number of useful abstractions that will serve as a starting 
point for front-end development. Any part of the code that is unnecessary for your purpose should be discarded and replaced with something more 
suited to your needs.</p>
<h2>In this guide</h2>
<p>All of the documentation for this project is kept as pages in this guide. Click the links to learn more</p>
'; ?>