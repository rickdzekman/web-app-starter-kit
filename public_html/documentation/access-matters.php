<?php
$accessMatters = '
<h2>First of all - let\'s be decent human beings</h2>
<p>In general the only argument that&nbsp;<strong>should</strong>&nbsp;matter is that some of our users have accessibility needs and you should build your web content in such a way that it is accessible.&nbsp;<a href="http://bradfrostweb.com/blog/post/fuck-you/">Brad Frost\'s excellent article Fuck You</a>&nbsp;says much more than I ever could about this issue.</p>
<h2>Accessibility is not just for disabled people</h2>
<p>It\'s easy to ignore disabled people if you don\'t know any personally. But there are plenty of things we might be able to empathise with to help get you to understand.</p>
<p>As part of my job I spend a lot of time on computers and a then some of my hobbies involve me staring at a screen too. By the end of the day my eyes get tired so I actually view all websites zoomed in at 120%-150% zoom. Websites with poor accessibility are harder for me to use and as a result I am less engaged with.</p>
<p>A good friend of mine suffers from severe RSI and is much more likely to navigate your website by keyboard than mouse - if he can. When you don\'t treat :focus the same way as :hover and when you remove outlines from focus elements you make it hard for him to use a website or web-app.</p>
<p>My little cousin has Autism and is an avid consumer of technology. I would hate to think that someone is willing to make a website visually difficult to use for people with cognitive difficulties. Keeping your links and buttons consistent, having a visual hierarchy and an ordered structure to your content are vital for people with cognitive difficulties.</p>
<h2>The selfish reasons</h2>
<p>OK let\'s suppose you have no care for the blind, handicapped or the disabled. Let\'s say that you also don\'t give a damn about my poor eyesight or my friend with RSI. There are some selfish reasons that accessibility matters too.</p>
<h3>Search Engines</h3>
<p>If you structure your website in such a way that people with assistive technologies can use your site it will have better SEO. Why? Google uses a bot to crawl your website much like a screen reader. If you structure your markup in such a way that is easy for a screen reader to pick up on you also structure it in such a way for search engine crawlers too.</p>
<h3>Future Proof</h3>
<p>We are not that far away from brain interface devices - at least simplistic ones that work on an EEG, of which we have large scale ones already. We already have voice interface devices like Glass and Siri. These technologies are not going to interface with your website the way a human would. Making your code accessible friendly makes it more future proof for these technologies. And that is just what we know about - we have no idea what technology is to come. Plan for the future by being accessible now.</p>
'; ?>