<?php
$ajax = '
<h2>In this section</h2>
<ul>
    <li><a href="/page/ajax-class/" class="ajax">The ajax class</a></li>
    <li><a href="/page/backbutton/" class="ajax">Browser back button</a></li>
    <li><a href="/page/ajax-popstate/" class="ajax">Tackling browser popstate issues</a></li>
</ul>
'; ?>