<?php
$backbutton = '
<h2>Pressing the back button</h2>
<p>In AJAX driven web applications users are constantly faced with a consistent problem: thy can\'t press the back button. In order to assist with that 
this web-app starter kit provides a number of useful things:</p>
<ol>
<li><strong>History.js</strong> - this uses a slightly older version of <a href="https://github.com/browserstate/history.js/">this history.js plugin</a></li>
<li><strong>UTF-8 Modification</strong> - the plugin was modified as per 
<a href="http://stackoverflow.com/questions/11068907/html5-history-pushstate-mangles-urls-containing-percent-encoded-non-ascii-unic">this Stack Overflow question
</a> to account for an issues whereby UTF-8 characters were not properly decoded.</li>
<li><strong>An abort intercept</strong> - a global variable in custom.js "getInProgress" keeps track of active AJAX requests and will abort one that 
is currently in progress if a new one starts; just in case the user hits back multiple times quickly</li>
</ol>
'; ?>