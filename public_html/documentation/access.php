<?php
$access = '
<h2>In this section</h2>
<ul>
    <li><a href="/page/accessibility-matters/" class="ajax">Why accessibility matters</a></li>
    <li><a href="/page/accessibility-structure/" class="ajax">Content Structure</a></li>
    <li><a href="/page/accessibility-legacy/" class="ajax">Older browsers</a></li>
</ul>
'; ?>