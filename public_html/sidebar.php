<!-- placeholder sidebar -->
<h2>Sidebar goes here</h2>
<ul>
    <li><a href="/page/about" class="ajax">About</a></li>
    <ul>
        <li><a href="/page/about-general" class="ajax">Why this starter kit</a></li>
        <li><a href="/page/about-requirements" class="ajax">Requirements</a></li>
    </ul>
    <li><a href="/page/ajax" class="ajax">AJAX</a></li>
    <ul>
        <li><a href="/page/ajax-class/" class="ajax">The ajax class</a></li>
        <li><a href="/page/backbutton" class="ajax">The browser back button</a></li>
        <li><a href="/page/ajax-popstate/" class="ajax">Browser popstate</a></li>
    </ul>
    <li><a href="/page/accessibility" class="ajax">Accessibility</a></li>
    <ul>
        <li><a href="/page/accessibility-matters/" class="ajax">Why accessibility matters</a></li>
        <li><a href="/page/accessibility-structure/" class="ajax">Content Structure</a></li>
        <li><a href="/page/accessibility-legacy/" class="ajax">Older browsers</a></li>
    </ul>
    <li><a href="/page/other" class="ajax">Other Components</a></li>
    <li><a href="/page/license" class="ajax">Available under MIT License</a></li>
</ul>