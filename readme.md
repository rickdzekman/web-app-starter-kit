#Requirements
* The HTML/CSS/JS is agnostic to the server side language but the demo will need PHP
* URL Rewriting is required for hisory.js to work properly on older browsers (a working .htaccess file is included for query string re-writes)
* Non-Javascript fallback - it is expected that all AJAX URLs would work even without JS

#Quick Start - Demo (Needs: Apache, PHP)
* Unpack all the source files
* Ensure you have a virtual host pointed to /your-app/public_html with an appropriate alias
* Make sure mod_rewrite is enabled
* Once that's done you should be able to visit your-app.tld and start running the demo. Try clicking around the links and then pressing back/forward/refresh a few times to see what happens.

#Quick Start - Only the AJAX utilities
* Add frameworks.min.js and custom.js to your project
* Ensure you have an element with id="main", which will be the target for any AJAX updates
* add class="ajax" to any link which will load dynamically

#Extending the functionality
To add extra functionality simply call the function dynamicLoadPage(page) in custom.js. For example - in whatistheretodo.com we use a search form which has an AJAX auto complete to identify a city. We capture the form submission with a "return false;" and instead process the information in the Javascript layer. Once we identify which page to send the user to we simpy call dynamicLoadPage(newurl);

In AJAX driven web applications users are constantly faced with a consistent problem: thy can't press the back button. In order to assist with that this web-app starter kit provides a number of userful things:

1. History.js - this uses a slightly older version of the history.js plugin
1. UTF-8 Modification - the plugin was modified to account for an issues whereby UTF-8 characters were not properly decoded.
1. An abort intercept - a global variable in custom.js "getInProgress" keeps track of active AJAX requests and will abort one that is currently in progress if a new one starts; just in case the user hits back multiple times quickly


#Further documentation
Full documentation is included as content within the demo.